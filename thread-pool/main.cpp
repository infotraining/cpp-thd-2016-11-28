#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <random>
#include "thread_safe_queue.hpp"

using namespace std;

using Task = function<void()>;

class ThreadPool
{
public:
    ThreadPool(size_t size) : threads_(size)
    {
        for(size_t i = 0; i < size; ++i)
        {
            threads_[i] = thread{[this] { run(); }};
        }
    }

    ThreadPool(const ThreadPool&) = delete;
    ThreadPool& operator=(const ThreadPool&) = delete;

    ~ThreadPool()
    {
        for(size_t i = 0; i < threads_.size(); ++i)
            tasks_.push(end_of_work);

        for(auto& t : threads_)
            t.join();
    }

    void submit(Task task)
    {
        if (!task)
            throw std::invalid_argument("Task cannot be empty.");

        tasks_.push(task);
    }

private:
    void run()
    {
        while(true)
        {
            Task task;
            tasks_.pop(task);

            if (task == end_of_work)
                return;

            task(); // calling a task
        }
    }

    std::vector<std::thread> threads_;
    ThreadSafeQueue<Task> tasks_;
    static const nullptr_t end_of_work;
};

void background_work(int id)
{
    cout << "BW#" << id << " has started..." << endl;

    random_device rd;
    mt19937_64 rnd_gen(rd());
    uniform_int_distribution<> distr(100, 2000);

    auto interval = chrono::milliseconds(distr(rnd_gen));
    this_thread::sleep_for(interval);

    cout << "BW#" << id << " is finished..." << endl;
}

int main()
{
    ThreadPool thread_pool(thread::hardware_concurrency());

    for(int i = 1; i <= 20; ++i)
    {
//        Task task = [i] { background_work(i); };
//        thread_pool.submit(task);

        thread_pool.submit([i] { background_work(i); });
    }
}
