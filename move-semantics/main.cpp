#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <algorithm>

using namespace std;

string foo(int x)
{
    return to_string(x);
}

class Array
{
    int* array_;
    size_t size_;
public:
    Array(size_t size) : array_{new int[size]}, size_{size}
    {
        cout << "Array(ctor: " << size_ << ")" << endl;
        fill_n(array_, size_, 0);
    }

    // copy constructor
    Array(const Array& source) : array_{new int[source.size()]}, size_{source.size()}
    {
        cout << "Array(cpy ctor: " << size_ << ")" << endl;
        copy_n(source.array_, size_, array_);
    }

    // move constructor
    Array(Array&& source) noexcept : array_{source.array_}, size_{source.size_}
    {
        cout << "Array(mv ctor: " << size_ << ")" << endl;

        source.array_ = nullptr;
        source.size_ = 0;
    }

    // move assignment
    Array& operator=(Array&& source) noexcept
    {
        cout << "Array=(mv: " << source.size() << ")\n";

        if (this != &source)
        {
            array_ = source.array_;
            size_ = source.size_;

            source.array_ = nullptr;
            source.size_ = 0;
        }

        return *this;
    }

    // copy assignment
    Array& operator=(const Array& source)
    {
        cout << "Array=(cpy: " << size_ << ")" << endl;

        if (this != &source)
        {
            int* temp_arr = new int[source.size()];
            copy_n(source.array_, size_, temp_arr);

            delete [] array_;
            array_ = temp_arr;
            size_ = source.size();
        }

        return *this;
    }

    ~Array()
    {
        cout << "~Array(" << size_ << ")" << endl;
        delete [] array_;
    }

    int& operator[](size_t index)
    {
        return array_[index];
    }

    const int& operator[](size_t index) const
    {
        return array_[index];
    }

    size_t size() const
    {
        return size_;
    }
};

Array create_array(size_t size)
{
    Array arr(size);

    for(size_t i = 0; i < arr.size(); ++i)
        arr[i] = i * 2;

    return arr;
}

// highly optimized class implementation
class Data
{
    Array array_;
public:
    Data(size_t size) : array_(size)
    {}

    void print() const
    {
        for(size_t i = 0; i < array_.size(); ++i)
            cout << array_[i] << " ";
        cout << endl;
    }
};


int main()
{
    Array arr1(10);

    for(size_t i = 0; i < arr1.size(); ++i)
        cout << arr1[i] << " ";
    cout << endl;

    Array arr2 = arr1;

    Array arr3 = create_array(100);

    cout << "\n--------\n";

    vector<Array> vec;
    vec.push_back(create_array(200));
    vec.push_back(create_array(300));
    vec.push_back(move(arr3));

    cout << "\n--------\n";

    vector<Data> data;

    data.push_back(Data(10));
    data.emplace_back(20);
    data.emplace_back(30);
}
