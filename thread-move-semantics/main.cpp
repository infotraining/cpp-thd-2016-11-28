#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>

using namespace std;

void background_work(int id, chrono::milliseconds interval)
{
    string text = "Hello from thread";

    for(const auto& c : text)
    {
        cout << "THD#" << id << ": " << c << endl;
        this_thread::sleep_for(interval);
    }

    cout << "END of THD#" << id << endl;
}

thread create_thread()
{
    static int gen_id;

    return thread{&background_work, ++gen_id, 200ms};
}

int main()
{
    auto hardware_thread_count = std::max(1u, thread::hardware_concurrency());

    cout << "hardware_thread_count: " << hardware_thread_count << endl;

    thread thd = create_thread();

    vector<thread> threads;

    threads.push_back(create_thread());
    threads.push_back(move(thd));
    threads.push_back(thread{&background_work, 13, 500ms});
    threads.emplace_back(&background_work, 665, 600ms);

    for(auto& t : threads)
        t.join();
}
