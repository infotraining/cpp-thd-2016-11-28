#include <iostream>
#include <thread>
#include <mutex>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>

using namespace std;

void background_work(int id, chrono::milliseconds timeout, timed_mutex& mtx)
{
    cout << "THD#" << id << " is waiting for mutex..." << endl;

    unique_lock<timed_mutex> lk{mtx, try_to_lock};

    if (!lk.owns_lock())
    {
        do
        {
            cout << "THD#" << id << " deosn't own lock..."
                 << " Attempt to acquire mutex..." << endl;
        } while (!lk.try_lock_for(timeout));
    }

    cout << "THD#" << id << " starts" << endl;

    for(int i = 0; i < 10; ++i)
    {
        cout << "THD#" << id << " is working..." << endl;
        this_thread::sleep_for(1s);
    }

    cout << "End of THD#" << id << endl;
}

int main()
{
    timed_mutex mtx;

    thread thd1{ &background_work, 1, 250ms, ref(mtx) };
    thread thd2{ &background_work, 2, 550ms, ref(mtx) };

    thd1.join();
    thd2.join();
}
