#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <string>

using namespace std;

void may_throw(int id, int arg)
{
    if (arg == 13)
        throw runtime_error("Error#13 from THD#" + to_string(id));
    else if (arg == 42)
        throw invalid_argument("Error#42 from THD#" + to_string(id));

}

void background_work(int id, int steps, const chrono::milliseconds& interval, exception_ptr& eptr)
{
    cout << "Start of THD#" << id << endl;

    try
    {
        for(int i = 0; i < steps; ++i)
        {
            cout << "THD#" << id << ": " << i << endl;
            this_thread::sleep_for(interval);

            may_throw(id, id % 2 ? i : i*2);
        }
    }
    catch(...)
    {
        eptr = current_exception();
    }

    cout << "END of THD#" << id << endl;
}

int main() try
{
    exception_ptr eptr;
    auto delay = 200ms;
    thread thd{&background_work, 1, 30, cref(delay), ref(eptr)};

    thd.join();

    try
    {
        if (eptr)
        {
            rethrow_exception(eptr);
        }
    }
    catch(const runtime_error& e)
    {
        cout << "Caught a runtime_error: " << e.what() << endl;
    }
    catch(const invalid_argument& e)
    {
        cout << "Caught an invalid argument: " << e.what() << endl;
    }

    cout << "\n\n------------------\n\n";

    const int no_of_threads = 10;

    vector<thread> threads(no_of_threads);
    vector<exception_ptr> eptrs(no_of_threads);

    for(int i = 0; i < no_of_threads; ++i)
    {
        threads[i] = thread{&background_work, i + 1, (i + 1) * 10, 100ms, ref(eptrs[i])};
    }

    for(auto& t : threads)
        t.join();

    // errror handling
    for(auto& eptr : eptrs)
    {
        try
        {
            if (eptr)
            {
                rethrow_exception(eptr);
            }
        }
        catch(const runtime_error& e)
        {
            cout << "Caught a runtime error: " << e.what() << endl;
        }
        catch(const invalid_argument& e)
        {
            cout << "Caught an invalid arg: " << e.what() << endl;
        }
    }


}
catch(const exception& e)
{
    cout << "Caught an exception: " << e.what() << endl;
}



