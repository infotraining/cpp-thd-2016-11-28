#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <mutex>
#include <atomic>

using namespace std;

namespace Leaky
{
    class Singleton
    {
    private:
        Singleton()
        {
            cout << "Singleton() - " << this << endl;
        }

        static atomic<Singleton*> instance_;
        static mutex mtx_;
    public:
        static Singleton& instance()
        {
            // double checked locking pattern
            if (!instance_)
            {
                lock_guard<mutex> lk{mtx_};

                if (!instance_)
                {
                    Singleton *temp = new Singleton();
                    instance_ = temp; // store with release semantics

                    //                //instance_ = new Singleton();
                    //                // 1 - alokacja pamieci
                    //                void* raw_mem = ::operator new(sizeof(Singleton));
                    //                // 2 - wywolanie konstruktora
                    //                new (raw_mem)Singleton();
                    //                // 3 - przypisanie do wskaznika
                    //                instance_ = static_cast<Singleton*>(raw_mem);

                }
            }

            return *instance_;
        }

        ~Singleton()
        {
            cout << "~Singleton()" << endl;
        }

        Singleton(const Singleton&) = delete;
        Singleton& operator=(const Singleton&) = delete;

        void do_stuff()
        {
            cout << "Singleton::do_stuff() - " << this << endl;
        }
    };

    atomic<Singleton*> Singleton::instance_;
    mutex Singleton::mtx_;
}

namespace CallOnce
{
    class Singleton
    {
    private:
        static std::unique_ptr<Singleton> instance_;
        static std::once_flag init_flag_;

        Singleton()
        {
            cout << "Singleton() - " << this << endl;
        }

    public:
        static Singleton& instance()
        {
            std::call_once(init_flag_, [] { instance_.reset(new Singleton()); });

            return *instance_;
        }

        ~Singleton()
        {
            cout << "~Singleton()" << endl;
        }

        Singleton(const Singleton&) = delete;
        Singleton& operator=(const Singleton&) = delete;

        void do_stuff()
        {
            cout << "Singleton::do_stuff() - " << this << endl;
        }
    };

    std::unique_ptr<Singleton> Singleton::instance_;
    std::once_flag Singleton::init_flag_;
}

namespace Meyers
{
    class Singleton
    {
    private:
        Singleton()
        {
            cout << "Singleton() - " << this << endl;
        }

    public:
        static Singleton& instance()
        {
            static Singleton unique_instance_;

            return unique_instance_;
        }

        ~Singleton()
        {
            cout << "~Singleton()" << endl;
        }

        Singleton(const Singleton&) = delete;
        Singleton& operator=(const Singleton&) = delete;

        void do_stuff()
        {
            cout << "Singleton::do_stuff() - " << this << endl;
        }
    };
}

int main()
{
    using namespace CallOnce;

    cout << "Start main..." << endl;

    thread thd{[] { Singleton::instance().do_stuff(); } };

    Singleton::instance().do_stuff();
    Singleton& s = Singleton::instance();
    s.do_stuff();

    thd.join();
}
