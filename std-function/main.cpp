#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>

using namespace std;

void foo(int x)
{
    cout << "foo(x: " << x << ")" << endl;
}

class Foo
{
public:
    void operator()(int x)
    {
        cout << "Foo.operator()(x: " << x << ")" << endl;
    }
};

class X
{
public:
    void foo(int x)
    {
        cout << "X::foo(x: " << x << ")\n";
    }
};

class Printer
{
public:
    using Callback = function<void(string)>;
    Callback on_start;

    void start()
    {
        on_start("Printer has started"); // calling callback
    }
};

class Logger
{
public:
    void log(string msg)
    {
        cout << "Logging " << msg << endl;
    }
};

int main()
{
    // 1 - function pointer
    void(*ptrf)(int) = &foo;
    ptrf(10); // call

    // 2 - functor
    Foo foobar;
    foobar(13); // call

    // 3 - lambda
    auto lfoo = [](int x) { cout << "Lambda(x: " << x << ")\n"; };
    lfoo(15); // call

    cout << "\n\n----------\n";

    function<void(int)> f;
    // f(20); // throws bad_function_call

    f = &foo;
    f(45);

    f = foobar;
    f(88);

    f = lfoo;
    f(99);

    function<void(X, int)> f2;
    f2 = &X::foo;
    X x;
    f2(x, 665);

    cout << "\n\n----------\n";

    Printer prn;
    Logger logger;

    prn.on_start = [&logger](string msg) { logger.log(msg); }; // setting up a callback

    //...
    prn.start();
}








