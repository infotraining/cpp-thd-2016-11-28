#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <random>
#include <future>

using namespace std;

void may_throw(int x)
{
    if (x == 13)
    {
        throw runtime_error("Error#13");
    }
}

int calculate_square(int x)
{
    cout << "Starting calculation for: " << x << endl;

    random_device rd;
    mt19937_64 rnd_gen(rd());
    uniform_int_distribution<> distr(100, 2000);

    auto interval = chrono::milliseconds(distr(rnd_gen));
    this_thread::sleep_for(interval);

    may_throw(x);

    return x * x;
}

void save_to_file(const string& filename)
{
    cout << "Saving to file: " << filename << endl;

    this_thread::sleep_for(2s);

    cout << "File " << filename << " saved" << endl;
}

class SquareCalculator
{
    promise<int> promise_;
public:
    future<int> get_future()
    {
        return promise_.get_future();
    }

    void operator()(int x)
    {
        cout << "Starting calculations in SquareCalculator: " << x << endl;

        this_thread::sleep_for(2s);

        try
        {
            may_throw(x);
        }
        catch (...)
        {
            auto e = current_exception();
            promise_.set_exception(e);

            return;
        }

        promise_.set_value(x * x);
    }
};

template <typename Callable>
auto launch_async(Callable&& c)
{
    using ResultType = decltype(c());
    packaged_task<ResultType()> pt(std::forward<Callable>(c));

    future<ResultType> f = pt.get_future();

    thread thd{move(pt)};
    thd.detach();

    return f;
}


void async_wtf()
{
    async(launch::async, &save_to_file, "data2.txt");
    async(launch::async, &save_to_file, "data3.txt");
    async(launch::async, &save_to_file, "data4.txt");
    async(launch::async, &save_to_file, "data5.txt");
}

void async_ok()
{
    launch_async([] { save_to_file("data2.txt"); });
    launch_async([] { save_to_file("data3.txt"); });
    launch_async([] { save_to_file("data4.txt"); });
    launch_async([] { save_to_file("data5.txt"); });
}

int main()
{
    // 1-st option
    packaged_task<int()> pt1{ [] { return calculate_square(8); } };
    packaged_task<int(int)> pt2{&calculate_square};
    packaged_task<void()> pt3{ [] { save_to_file("data1.txt"); } };

    future<int> f1 = pt1.get_future();
    future<int> f2 = pt2.get_future();
    future<void> f3 = pt3.get_future();

    thread thd1{ move(pt1) };
    thread thd2{ move(pt2), 13 };
    thread thd3 { move(pt3) };

    while( f1.wait_for(100ms) != future_status::ready)
    {
        cout << "main is waiting for a result..." << endl;
    }

    cout << "result1: " << f1.get() << endl;
    try
    {
        cout << "result2: " << f2.get() << endl;
    }
    catch(const runtime_error& e)
    {
        cout << "Caught an exception: " << e.what() << endl;
    }

    f3.wait();

    thd1.join();
    thd2.join();
    thd3.join();

    // 2nd option
    SquareCalculator calc;

    future<int> f4 = calc.get_future();

    thread thd4{ ref(calc), 13 };
    thd4.detach();

    try
    {
        cout << "result4: " << f4.get() << endl;
    }
    catch(const runtime_error& e)
    {
        cout << "Caught an exception: " << e.what() << endl;
    }

    // 3rd option
    future<int> f5 = async(launch::async, &calculate_square, 8);
    cout << "hello from main..." << endl;

    vector<future<int>> future_squares;

    for(int i = 1; i < 20; ++i)
        future_squares.push_back(async(launch::async, &calculate_square, i));

    future_squares.push_back(move(f5));

    cout << "results: ";
    for(auto& fs : future_squares)
    {
        try
        {
            cout << fs.get() << " ";
        }
        catch (const runtime_error& e)
        {
            cout << e.what() << " ";
        }
    }

    cout << "\n\n--------------\n";
    async_wtf();

    cout << "\n\n--------------\n";
    async_ok();

    this_thread::sleep_for(5s);
}











