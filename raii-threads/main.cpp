#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>

using namespace std;

void background_work(int id, chrono::milliseconds interval)
{
    string text = "Hello from thread";

    for(const auto& c : text)
    {
        cout << "THD#" << id << ": " << c << endl;
        this_thread::sleep_for(interval);
    }

    cout << "END of THD#" << id << endl;
}

void may_throw()
{
    throw runtime_error("Error#13");
}

namespace Utils
{
    namespace WithRef
    {
        class RaiiThread
        {
            thread& thd_;
        public:
            RaiiThread(thread& thd) : thd_{thd}
            {}

            RaiiThread(const RaiiThread&) = delete;
            RaiiThread& operator=(const RaiiThread&) = delete;

            thread& get()
            {
                return thd_;
            }

            ~RaiiThread()
            {
                if (thd_.joinable())
                    thd_.join();
            }
        };

        class DetachedThread
        {
            thread& thd_;
        public:
            DetachedThread(thread& thd) : thd_{thd}
            {}

            DetachedThread(const DetachedThread&) = delete;
            DetachedThread& operator=(const DetachedThread&) = delete;

            thread& get()
            {
                return thd_;
            }

            ~DetachedThread()
            {
                if (thd_.joinable())
                    thd_.detach();
            }
        };
    }

    class RaiiThread
    {
        thread thd_;
    public:
        // perfect forwarding constructor
        template <typename... Args>
        RaiiThread(Args&&... args) : thd_{forward<Args>(args)...}
        {}

        RaiiThread(const RaiiThread&) = delete;
        RaiiThread& operator=(const RaiiThread&) = delete;

        ~RaiiThread()
        {
            if (thd_.joinable())
                thd_.join();
        }

        thread& get()
        {
            return thd_;
        }
    };

    class DetachedThread
    {
        thread thd_;
    public:
        template <typename... Args>
        DetachedThread(Args&&... args) : thd_{forward<Args>(args)...}
        {}

        DetachedThread(const DetachedThread&) = delete;
        DetachedThread& operator=(const DetachedThread&) = delete;

        thread& get()
        {
            return thd_;
        }

        ~DetachedThread()
        {
            if (thd_.joinable())
                thd_.detach();
        }
    };
}

int main() try
{
    using namespace Utils;

    //    thread thd1{&background_work, 1, 500ms};
    //    RaiiThread raii_thd1{thd1};

    RaiiThread thd1{&background_work, 1, 500ms};

    //    thread thd2{&background_work, 2, 1000ms};
    //    DetachedThread detached_thread{thd2};

    DetachedThread detached_thread{&background_work, 2, 1000ms};

    // RaiiThread raii_thd2 = raii_thd1;  // error - RaiiThread is non-copyable

    may_throw();
}
catch(const runtime_error& e)
{
    cout << "Caught an exception: " << e.what() << endl;
}
