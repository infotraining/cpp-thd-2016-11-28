#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>

using namespace std;

void hello(unsigned int id, chrono::milliseconds interval)
{
    string text = "Hello from thread";

    for(const auto& c : text)
    {
        cout << "THD#" << id << ": " << c << endl;
        this_thread::sleep_for(interval);
    }

    cout << "END of THD#" << id << endl;
}

class BackgroundWorker
{
    const unsigned int id_;
public:
    BackgroundWorker(unsigned int id) : id_{id}
    {}

    void operator()(chrono::milliseconds interval)
    {
        for(int i = 0; i < 10; ++i)
        {
            cout << "BW#" << id_ << ": " << i << endl;
            this_thread::sleep_for(interval);
        }

        cout << "END of BW#" << id_ << endl;
    }
};

int main()
{
    thread thd_0;
    cout << "Thread id: " << thd_0.get_id() << endl;

    thread thd_1{&hello, 1, chrono::milliseconds(500)};
    thread thd_2{&hello, 2, 250ms}; // C++14

    BackgroundWorker bw{3};
    thread thd_3{bw, 300ms};
    thread thd_4{BackgroundWorker{4}, 400ms};

    const vector<int> data = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 }; // std::initializer_list<int>
    vector<int> target1(data.size());
    vector<int> target2(data.size());

    thread thd_5{ [&data, &target1] { copy(data.begin(), data.end(), target1.begin()); } };
    thread thd_6{ [&data, &target2] { copy(data.begin(), data.end(), target2.begin()); } };

    thd_1.detach();
    assert(!thd_1.joinable());

    thd_2.join();
    thd_3.join();
    thd_4.join();
    thd_5.join();
    thd_6.join();
}
