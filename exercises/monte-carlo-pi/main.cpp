#include <iostream>
#include <random>
#include <chrono>
#include <vector>
#include <thread>
#include <atomic>
#include <numeric>
#include <mutex>
#include <future>

using namespace std;

unsigned long long calculate_hits(unsigned long long n)
{
    random_device rd;
    mt19937_64 gen(rd());
    uniform_real_distribution<> dis(-1, 1);

    unsigned long long counter = 0;

    for (auto i = 0ull ; i < n ; ++i)
    {
        double x = dis(gen);
        double y = dis(gen);
        if (x*x + y*y < 1)
            counter++;
    }

    return counter;
}

void calc_hits(unsigned long long n, unsigned long long& hits)
{
    random_device rd;
    mt19937_64 gen(rd());
    uniform_real_distribution<> dis(-1, 1);

    unsigned long long counter = 0;

    for (auto i = 0ull ; i < n ; ++i)
    {
        double x = dis(gen);
        double y = dis(gen);
        if (x*x + y*y < 1)
            counter++;
    }

    hits += counter;
}

template <typename T>
void calc_hits_with_mutex(T n, T& hits, mutex& mtx)
{
    random_device rd;
    mt19937_64 gen(rd());
    uniform_real_distribution<> dis(-1, 1);

    for (auto i = 0ull ; i < n ; ++i)
    {
        double x = dis(gen);
        double y = dis(gen);
        if (x*x + y*y < 1)
        {
            lock_guard<mutex> lk{mtx}; // konstrukcja obiektu raii (lock())
            hits++;
        }
    }
}

void calc_hits_with_atomic(unsigned long long n, atomic<unsigned long long>& hits)
{
    random_device rd;
    mt19937_64 gen(rd());
    uniform_real_distribution<> dis(-1, 1);

    for (auto i = 0ull ; i < n ; ++i)
    {
        double x = dis(gen);
        double y = dis(gen);
        if (x*x + y*y < 1)
        {
            hits++;
        }
    }
}

void pi_single_threaded(unsigned long long n)
{
    cout << "Pi on single thread calc!" << endl;
    auto start = chrono::high_resolution_clock::now();

    unsigned long long hits = 0;
    calc_hits(n, hits);

    auto end = chrono::high_resolution_clock::now();

    cout << "Pi on single thread = " << (4.0 * hits / n) << endl;
    cout << "Elapsed = ";
    cout << chrono::duration_cast<chrono::milliseconds>(end-start).count();
    cout << " ms" << endl;
}

void pi_multithreading(unsigned long long n)
{
    cout << "Pi multithreading calc!" << endl;
    auto start = chrono::high_resolution_clock::now();

    const unsigned int hardware_thread_count = max(1u, thread::hardware_concurrency());

    vector<thread> threads(hardware_thread_count);
    vector<unsigned long long> partial_hits(hardware_thread_count);

    auto no_of_attempts = n / hardware_thread_count;

    for(size_t i = 0; i < hardware_thread_count; ++i)
        threads[i] = thread{[i, no_of_attempts, &partial_hits] { calc_hits(no_of_attempts, partial_hits[i]); } };

    for(auto& t : threads)
        t.join();

    unsigned long long hits = accumulate(partial_hits.begin(), partial_hits.end(), 0ull);

    auto end = chrono::high_resolution_clock::now();

    cout << "Pi multithreading = " << (4.0 * hits / n) << endl;
    cout << "Elapsed = ";
    cout << chrono::duration_cast<chrono::milliseconds>(end-start).count();
    cout << " ms" << endl;
}

void pi_with_mutex(unsigned long long n)
{
    cout << "Pi with mutex multithreading calc!" << endl;
    auto start = chrono::high_resolution_clock::now();

    const unsigned int hardware_thread_count = max(1u, thread::hardware_concurrency());

    vector<thread> threads(hardware_thread_count);
    auto no_of_attempts = n / hardware_thread_count;
    unsigned long long hits = 0;
    mutex mtx;

    for(size_t i = 0; i < hardware_thread_count; ++i)
        threads[i] = thread{[no_of_attempts, &hits, &mtx] { calc_hits_with_mutex(no_of_attempts, hits, mtx); } };

    for(auto& t : threads)
        t.join();

    auto end = chrono::high_resolution_clock::now();

    cout << "Pi with mutex = " << (4.0 * hits / n) << endl;
    cout << "Elapsed = ";
    cout << chrono::duration_cast<chrono::milliseconds>(end-start).count();
    cout << " ms" << endl;
}

void pi_with_atomic(unsigned long long n)
{
    cout << "Pi with atomic multithreading calc!" << endl;
    auto start = chrono::high_resolution_clock::now();

    const unsigned int hardware_thread_count = max(1u, thread::hardware_concurrency());

    vector<thread> threads(hardware_thread_count);
    auto no_of_attempts = n / hardware_thread_count;
    atomic<unsigned long long> hits{};

    for(size_t i = 0; i < hardware_thread_count; ++i)
        threads[i] = thread{[no_of_attempts, &hits] { calc_hits_with_atomic(no_of_attempts, hits); } };

    for(auto& t : threads)
        t.join();

    auto end = chrono::high_resolution_clock::now();

    cout << "Pi with atomic = " << (4.0 * hits / n) << endl;
    cout << "Elapsed = ";
    cout << chrono::duration_cast<chrono::milliseconds>(end-start).count();
    cout << " ms" << endl;
}

void pi_with_futures(unsigned long long n)
{
    cout << "Pi with futures calc!" << endl;
    auto start = chrono::high_resolution_clock::now();

    const unsigned int hardware_thread_count = max(1u, thread::hardware_concurrency());

    auto no_of_attempts = n / hardware_thread_count;

    vector<future<unsigned long long>> partial_hits;

    for(size_t i = 0; i < hardware_thread_count; ++i)
        partial_hits.push_back(async(launch::async, &calculate_hits, no_of_attempts));

    unsigned long long hits = 0;
    for(auto& ph : partial_hits)
        hits += ph.get();

    auto end = chrono::high_resolution_clock::now();

    cout << "Pi with futures = " << (4.0 * hits / n) << endl;
    cout << "Elapsed = ";
    cout << chrono::duration_cast<chrono::milliseconds>(end-start).count();
    cout << " ms" << endl;
}

int main()
{
    const unsigned long long N = 30'000'000;

    pi_single_threaded(N);

    cout << "\n\n";

    pi_multithreading(N);

    cout << "\n\n";

    pi_with_mutex(N);

    cout << "\n\n";

    pi_with_atomic(N);

    cout << "\n\n";

    pi_with_futures(N);

    return 0;
}

