#ifndef ACTIVE_OBJECT_HPP
#define ACTIVE_OBJECT_HPP

#include <thread>
#include "thread_safe_queue.hpp"

using Task = std::function<void()>;

class ActiveObject
{
public:
    ActiveObject() : is_done_{false}
    {
        thd_ = std::thread{ &ActiveObject::run, this };
    }

    ActiveObject(const ActiveObject&) = delete;
    ActiveObject& operator=(const ActiveObject&) = delete;

    ~ActiveObject()
    {
        send([this] { is_done_ = true; });

        thd_.join();
    }

    void send(Task t)
    {
        q_.push(t);
    }

private:
    void run()
    {
        while(true)
        {
            Task task;
            q_.pop(task);

            task();

            if (is_done_)
                return;
        }
    }

    std::thread thd_;
    bool is_done_;
    ThreadSafeQueue<Task> q_;
};


#endif // ACTIVE_OBJECT_HPP
