#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <algorithm>
#include <random>
#include <atomic>
#include <mutex>
#include <condition_variable>

using namespace std;

namespace Atomic
{
    class Data
    {
        vector<int> data_;
        atomic<bool> is_ready_{};
    public:
        void read()
        {
            cout << "Start reading..." << endl;

            this_thread::sleep_for(2s);

            data_.resize(100);

            random_device rd;
            mt19937_64 rnd_gen{rd()};
            uniform_int_distribution<> rnd_distr{1, 100};

            generate(data_.begin(), data_.end(), [&] { return rnd_distr(rnd_gen); } );

            cout << "End of reading..." << endl;

            //is_ready_ = true;
            is_ready_.store(true, memory_order::memory_order_release);
        }

        void process(int id)
        {
            // busy wait
            while(!is_ready_.load(memory_order::memory_order_acquire))
            {
            }

            long sum = accumulate(data_.begin(), data_.end(), 0L);

            cout << "Id: " << id << "; Sum: " << sum << endl;
        }
    };

}

namespace CV
{
    class Data
    {
        vector<int> data_;
        bool is_ready_{};
        mutex mtx_;
        condition_variable cv_is_ready_;
    public:
        void read()
        {
            cout << "Start reading..." << endl;

            this_thread::sleep_for(2s);

            data_.resize(100);

            random_device rd;
            mt19937_64 rnd_gen{rd()};
            uniform_int_distribution<> rnd_distr{1, 100};

            generate(data_.begin(), data_.end(), [&] { return rnd_distr(rnd_gen); } );

            cout << "End of reading..." << endl;

            unique_lock<mutex> lk{mtx_};
            is_ready_ = true;
            lk.unlock();

            cv_is_ready_.notify_all();
        }

        void process(int id)
        {
            unique_lock<mutex> lk{mtx_};
            cv_is_ready_.wait(lk, [this] { return is_ready_; });

            lk.unlock();

            long sum = accumulate(data_.begin(), data_.end(), 0L);

            cout << "Id: " << id << "; Sum: " << sum << endl;
        }
    };
}

int main()
{
    using namespace CV;

    Data data;

    thread thd1{[&data] { data.read(); }};
    thread thd2{[&data] { data.process(1); }};
    thread thd3{[&data] { data.process(2); }};

    thd1.join();
    thd2.join();
    thd3.join();
}
