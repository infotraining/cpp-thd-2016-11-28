#ifndef THREAD_SAFE_QUEUE_HPP
#define THREAD_SAFE_QUEUE_HPP

#include <condition_variable>
#include <queue>
#include <mutex>
#include <queue>
#include <condition_variable>

template <typename T>
class ThreadSafeQueue
{
    std::queue<T> q_;
    mutable std::mutex mtx_;
    std::condition_variable cv_not_empty_;
public:
    bool empty() const
    {
        std::lock_guard<std::mutex> lk{mtx_};
        return q_.empty();
    }

    void push(const T& item)
    {
        {
            std::lock_guard<std::mutex> lk{mtx_};
            q_.push(item); // copying an item into q_
        }
        cv_not_empty_.notify_one();
    }

    void push(T&& item)
    {
        {
            std::lock_guard<std::mutex> lk{mtx_};
            q_.push(std::move(item)); // moving an item into q_
        }
        cv_not_empty_.notify_one();
    }

    void push(std::initializer_list<T> items)
    {
        {
            std::lock_guard<std::mutex> lk{mtx_};
            for(const auto& item : items)
                q_.push(item);
        }

        cv_not_empty_.notify_all();
    }

    bool try_pop(T& item)
    {
        std::unique_lock<std::mutex> lk{mtx_, std::try_to_lock};

        if (!lk || q_.empty())
            return false;

        item = std::move(q_.front());
        q_.pop();

        return true;
    }

    void pop(T& item)
    {
        std::unique_lock<std::mutex> lk{mtx_};
        cv_not_empty_.wait(lk, [this] { return !q_.empty(); });
        item = std::move(q_.front());
        q_.pop();
    }
};

#endif // THREAD_SAFE_QUEUE_HPP
