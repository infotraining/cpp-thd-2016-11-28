#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <algorithm>

using namespace std;

// closure class
class MagicLambda_2u34t2uy4r
{
public:
    void operator()() const
    {
        cout << "Lambda" << endl;
    }
};

class MagicLambda_35293852sdf
{
public:
    int operator()(int a, int b) const
    {
        return a + b;
    }
};

int main()
{
    auto l1 = [] { cout << "Lambda" << endl; };
    auto explained_l1 = MagicLambda_2u34t2uy4r();

    l1();
    explained_l1();

    auto add = [](int a, int b) { return a + b; };
    cout << "add(1, 2) = " << add(1, 2) << endl;

    vector<int> data = { 1, 2, 3, 4, 5 };

    // printing
    for_each(data.begin(), data.end(), [](int x) { cout << x << " ";});

    cout << endl;

    // sum all greater than threshold
    int sum = 0;
    int threshold = 2;

    for_each(data.begin(), data.end(), [&sum, threshold](int x) { if (x > threshold) sum += x; });
    cout << "sum = " << sum << endl;
}
